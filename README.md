# SOMOD

> SOMOD SDK : Create Sharable Module with Infrastructure + Code + UI bundled into one

With SOMOD SDK,

- Create Modules Containing your

  - Backend Infrastructure
  - Backend Code
  - UI Code

- The Modules can be sharable as NPM Packages

  - add child modules as npm dependencies
  - start by creating modules for smallest feature, and keep creating higher modules to integrate into complete product

- Scalable
  - from a landing page  
    To
  - Complete Application

## Documentation

https://somod.sodaru.com/
